<?php

namespace App\Tests\Application;

use App\Repository\UserRepository;
use \InvalidArgumentException;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class JwtWebTestCase extends WebTestCase
{
    private KernelBrowser $client;

    private ?string $userToken = null;

    private ?string $adminToken = null;

    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    protected function makeGuestRequest(string $method, string $url, array $data = []): Response
    {
        $this->client->request($method, $url, $data);

        return $this->client->getResponse();
    }

    protected function makeUserRequest(string $method, string $url, array $data = []): Response
    {
        $this->client->request($method, $url, $data, [], [
            'Authorization' => 'Bearer ' . $this->getUserJwtToken()
        ]);

        return $this->client->getResponse();
    }

    protected function makeAdminRequest(string $method, string $url, array $data = []): Response
    {
        $this->client->request($method, $url, $data, [], [
            'Authorization' => 'Bearer ' . $this->getAdminJwtToken()
        ]);

        return $this->client->getResponse();
    }

    private function getUserJwtToken(): string
    {
        if (!$this->userToken) {
            $this->userToken = $this->generateToken('user@user.com');
        }

        return $this->userToken;
    }

    private function getAdminJwtToken(): string
    {
        if (!$this->adminToken) {
            $this->adminToken = $this->generateToken('admin@admin.com');
        }

        return $this->adminToken;
    }

    private function generateToken(string $email): string
    {
        $userRepository = static::getContainer()->get(UserRepository::class);
        $JWTManager = static::getContainer()->get(JWTTokenManagerInterface::class);

        $user = $userRepository->findOneByEmail($email);

        if (!$user) {
            throw new InvalidArgumentException('Unable to find user by email ' . $email);
        }

        return $JWTManager->create($user);
    }
}