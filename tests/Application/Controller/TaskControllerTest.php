<?php

namespace App\Tests\Application\Controller;

use App\Tests\Application\JwtWebTestCase;

class TaskControllerTest extends JwtWebTestCase
{
    private string $taskContent = 'Test task';

    public function testTaskCreating()
    {
        $response = $this->makeUserRequest('POST', '/api/task', [
            'content' => $this->taskContent,
            'done' => 0
        ]);
        $data = json_decode($response->getContent(), true);

        $this->assertEquals('201', $response->getStatusCode());
        $this->assertArrayHasKey('content', $data);
        $this->assertArrayHasKey('isDone', $data);
        $this->assertArrayHasKey('createDate', $data);
        $this->assertArrayHasKey('updateDate', $data);
        $this->assertArrayHasKey('uuid', $data);

        $this->assertSame($this->taskContent, $data['content']);
        $this->assertSame(0, $data['isDone']);
    }

    public function testTaskUpdating()
    {
        $response = $this->makeUserRequest('PUT', '/api/task/' . $this->getTaskUuid(), [
            'done' => 1
        ]);
        $data = json_decode($response->getContent(), true);

        $this->assertEquals('200', $response->getStatusCode());
        $this->assertArrayHasKey('content', $data);
        $this->assertArrayHasKey('isDone', $data);
        $this->assertArrayHasKey('createDate', $data);
        $this->assertArrayHasKey('updateDate', $data);
        $this->assertArrayHasKey('uuid', $data);

        $this->assertSame($this->taskContent, $data['content']);
        $this->assertSame(0, $data['isDone']);
    }

    public function testTaskFetching()
    {
        $response = $this->makeUserRequest('GET', '/api/task/' . $this->getTaskUuid());
        $data = json_decode($response->getContent(), true);

        $this->assertEquals('200', $response->getStatusCode());
        $this->assertArrayHasKey('content', $data);
        $this->assertArrayHasKey('isDone', $data);
        $this->assertArrayHasKey('createDate', $data);
        $this->assertArrayHasKey('updateDate', $data);
        $this->assertArrayHasKey('uuid', $data);
    }

    public function testTasksFetching()
    {
        $response = $this->makeUserRequest('GET', '/api/task');
        $data = json_decode($response->getContent(), true)[0];

        $this->assertEquals('200', $response->getStatusCode());
        $this->assertArrayHasKey('content', $data);
        $this->assertArrayHasKey('isDone', $data);
        $this->assertArrayHasKey('createDate', $data);
        $this->assertArrayHasKey('updateDate', $data);
        $this->assertArrayHasKey('uuid', $data);
    }

    public function testTaskDeleting()
    {
        $response = $this->makeUserRequest('DELETE', '/api/task/' . $this->getTaskUuid());

        $this->assertEquals('204', $response->getStatusCode());
    }

    private function getTaskUuid(): string
    {
        //TODO

        return '';
    }
}