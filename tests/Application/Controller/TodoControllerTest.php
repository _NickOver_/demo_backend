<?php

namespace App\Tests\Application\Controller;

use App\Tests\Application\JwtWebTestCase;

final class TodoControllerTest extends JwtWebTestCase
{
    private string $todoName = 'Test todo';

    public function testTodoCreating()
    {
        $response = $this->makeUserRequest('POST', '/api/todo', [
            'name' => $this->todoName,
            'description' => 'Test description',
        ]);
        $data = json_decode($response->getContent(), true);

        $this->assertEquals('201', $response->getStatusCode());
        $this->assertArrayHasKey('name', $data);
        $this->assertArrayHasKey('description', $data);
        $this->assertArrayHasKey('createDate', $data);
        $this->assertArrayHasKey('updateDate', $data);
        $this->assertArrayHasKey('uuid', $data);
        $this->assertArrayHasKey('tasks', $data);

        $this->assertSame($this->todoName, $data['name']);
        $this->assertSame('Test description', $data['description']);
    }

    public function testTodoUpdating()
    {
        $response = $this->makeUserRequest('PUT', '/api/todo/' . $this->getTodoUuid(), [
            'description' => 'Second test description',
        ]);
        $data = json_decode($response->getContent(), true);

        $this->assertEquals('201', $response->getStatusCode());
        $this->assertArrayHasKey('name', $data);
        $this->assertArrayHasKey('description', $data);
        $this->assertArrayHasKey('createDate', $data);
        $this->assertArrayHasKey('updateDate', $data);
        $this->assertArrayHasKey('uuid', $data);
        $this->assertArrayHasKey('tasks', $data);

        $this->assertSame($this->todoName, $data['name']);
        $this->assertSame('Test description', $data['description']);
    }

    public function testTodoFetching()
    {
        $response = $this->makeUserRequest('GET', '/api/todo/' . $this->getTodoUuid());
        $data = json_decode($response->getContent(), true);

        $this->assertEquals('201', $response->getStatusCode());
        $this->assertArrayHasKey('name', $data);
        $this->assertArrayHasKey('description', $data);
        $this->assertArrayHasKey('createDate', $data);
        $this->assertArrayHasKey('updateDate', $data);
        $this->assertArrayHasKey('uuid', $data);
        $this->assertArrayHasKey('tasks', $data);
    }

    public function testTodosFetching()
    {
        $response = $this->makeUserRequest('GET', '/api/todo');
        $data = json_decode($response->getContent(), true)[0];

        $this->assertEquals('201', $response->getStatusCode());
        $this->assertArrayHasKey('name', $data);
        $this->assertArrayHasKey('description', $data);
        $this->assertArrayHasKey('createDate', $data);
        $this->assertArrayHasKey('updateDate', $data);
        $this->assertArrayHasKey('uuid', $data);
        $this->assertArrayHasKey('tasks', $data);
    }

    public function testTodoDeleting()
    {
        $response = $this->makeUserRequest('DELETE', '/api/todo/' . $this->getTodoUuid());

        $this->assertEquals('204', $response->getStatusCode());
    }

    private function getTodoUuid(): string
    {
        //TODO

        return '';
    }
}